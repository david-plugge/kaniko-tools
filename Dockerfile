# https://github.com/GoogleContainerTools/kaniko/blob/main/deploy/Dockerfile

ARG VERSION=debug
FROM gcr.io/kaniko-project/executor:${VERSION} as kaniko

FROM alpine:3.16

RUN apk add --update \
    bash curl git grep tar jq coreutils openssl ca-certificates

COPY --from=kaniko /kaniko /kaniko
COPY --from=kaniko /etc/nsswitch.conf /etc/nsswitch.conf

ENV HOME /root
ENV USER root
ENV PATH $PATH:/kaniko
ENV SSL_CERT_DIR=/kaniko/ssl/certs
ENV DOCKER_CONFIG /kaniko/.docker/
ENV DOCKER_CREDENTIAL_GCR_CONFIG /kaniko/.config/gcloud/docker_credential_gcr_config.json

WORKDIR /workspace

ENTRYPOINT ["/kaniko/executor"]